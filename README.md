# robles-io-logger

Python package for creating a root logger object.

Published to pypi at https://pypi.robles.io/simple/ ([more info](http://gitlab.com/carlosrobles/pypi-robles-io))

## Installation
```
pip install robles-io-logger --extra-index-url https://pypi.robles.io/simple/
```

## Usage
```
import robles_io_logger as logger

log = logger.get_logger()

log.info("info")
log.warning("warning")
log.error("error")
```
