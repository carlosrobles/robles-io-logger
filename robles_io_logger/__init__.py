"""robles-io-logger - Package for creating a root logger object"""

import logging
import sys

__version__ = '0.1.0'
__author__ = 'Carlos Robles <carlos@robles.io>'
__all__ = ['get_logger']


def get_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(name)s - %(levelname)s:    %(message)s')
    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(stream_handler)

    return logger
